﻿using Mark7.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Drawing;

namespace Mark7.Tests
{
    public class BaseTest
    {
        public IWebDriver driver;
        public LoginPage loginPage;
        public TaskPage taskPage;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();

            driver.Manage().Window.Size = new Size(1440, 900);
            driver.Navigate().GoToUrl("http://mark7.herokuapp.com/login");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            loginPage = new LoginPage(driver);
            taskPage = new TaskPage(driver);
        }

        [TearDown]
        public void Finish()
        {
            driver.Close();
        }
    }
}
