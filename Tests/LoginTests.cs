﻿using NUnit.Framework;

namespace Mark7.Tests
{
    public class LoginTests : BaseTest
    {
        [Test]
        public void DeveExibirSenhaInvalida()
        {
            loginPage.Logar("eu@papito.io", "abc123");
            Assert.AreEqual("Senha inválida.", loginPage.Notificacao().Text);
        }

        [Test]
        public void DeveExibirUsuarioNaoCadastrado()
        {
            loginPage.Logar("404@papito.io", "abc123");
            Assert.AreEqual("Usuário não cadastrado.", loginPage.Notificacao().Text);
        }

        [Test]
        public void DeveExibirEmailIncorreto()
        {
            loginPage.Logar("eu&papito.io", "abc123");
            Assert.AreEqual("Email incorreto ou ausente.", loginPage.Notificacao().Text);
        }

        [Test]
        public void DeveExibirEmailAusente()
        {
            loginPage.Logar("", "abc123");
            Assert.AreEqual("Email incorreto ou ausente.", loginPage.Notificacao().Text);
        }

        [Test]
        public void DeveExibirSenhaAusente()
        {
            loginPage.Logar("teste@email.com", "");
            Assert.AreEqual("Senha ausente.", loginPage.Notificacao().Text);
        }

        [Test]
        public void DeveAutenticarUsuario()
        {
            loginPage.Logar("eu@papito.io", "123456");
            Assert.AreEqual("Olá, Fernando", taskPage.BemVindo().Text);
        }
    }
}
