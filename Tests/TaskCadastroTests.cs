﻿using NUnit.Framework;

namespace Mark7.Tests
{
    public class TaskCadastroTests : BaseTest
    {
        [SetUp]
        public void Before()
        {
            loginPage.Logar("me@papito.io", "123456");
        }

        [Test]
        public void DeveCadastrarUmaTarefa()
        {
            var tarefa = new { Titulo = "Estudar CSharp - " + Faker.Name.First(), Data = "01/04/2019" };
            taskPage.Nova(tarefa.Titulo, tarefa.Data);

            var res = taskPage.RetornaRegistro(tarefa.Titulo);
            StringAssert.Contains(tarefa.Titulo, res.Text);
        }

        [Test]
        public void DeveExibirTarefaDuplicada()
        {
            var tarefa = new { Titulo = "Tarefa Duplicada - " + Faker.Name.First(), Data = "01/04/2019" };

            taskPage.Nova(tarefa.Titulo, tarefa.Data);
            taskPage.Nova(tarefa.Titulo, tarefa.Data);

            Assert.AreEqual("Tarefa duplicada.", taskPage.RetornaAlerta());
        }

        [Test]
        public void QuandoDuplicadoDeveExibirSomenteUmRegitro()
        {
            var tarefa = new { Titulo = "Registro Duplicado - " + Faker.Name.First(), Data = "01/04/2019" };

            taskPage.Nova(tarefa.Titulo, tarefa.Data);
            taskPage.Nova(tarefa.Titulo, tarefa.Data);

            taskPage.Voltar();
            taskPage.BuscaPorTitulo(tarefa.Titulo);

            Assert.AreEqual(1, taskPage.RetornaQuantidadeRegistros());
        }

        [Test]
        public void QuandoTituloEMuitCurtoDeveExibirAlerta()
        {
            var tarefa = new { Titulo = "asdfghjkl" , Data = "01/04/2019" };

            taskPage.Nova(tarefa.Titulo, tarefa.Data);

            Assert.AreEqual("10 caracteres é o mínimo permitido.", taskPage.RetornaAlerta());
        }
    }
}
