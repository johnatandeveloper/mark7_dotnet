﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Mark7.Tests
{
    public class TaskExclusaoTests : BaseTest
    {
        [SetUp]
        public void Before()
        {
            loginPage.Logar("me@papito.io", "123456");
        }

        [Test]
        public void DeveRemoverUmaTarefa()
        {
            var tarefa = new { Titulo = "Tarefa muito boa para ser removida", Data = "01/04/2019" };
            taskPage.Nova(tarefa.Titulo, tarefa.Data);

            taskPage.SolicitaExclusao(tarefa.Titulo);
            taskPage.ConfirmaExclusao();

            taskPage.BuscaPorTitulo(tarefa.Titulo);

            Assert.AreEqual("Hmm... nenhuma tarefa encontrada :(", taskPage.RetornaMensagemBusca().Text);
        }
    }
}
